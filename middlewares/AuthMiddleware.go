package middlewares

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func Auth(c *gin.Context) {
	authorizationHeader := c.Request.Header.Get("Authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
				if jwt.GetSigningMethod("HS256") != token.Method {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}

				return []byte("secret"), nil
			})

			if token != nil && err == nil {
				fmt.Println("token verified")
			} else {
				c.JSON(http.StatusUnauthorized, gin.H{
					"status":  "error",
					"message": err.Error(),
				})
				c.Abort()
			}

		} else {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status":  "error",
				"message": "Invalid authorization token.",
			})
			c.Abort()
		}

	} else {
		c.JSON(http.StatusUnauthorized, gin.H{
			"status":  "error",
			"message": "An authorization header is required.",
		})
		c.Abort()
	}
}
