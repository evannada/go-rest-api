package models

type Role struct {
	ID   int    `json:"id"`
	Name string `form:"name" json:"name" binding:"required"`
}
