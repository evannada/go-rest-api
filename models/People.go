package models

import "time"

type People struct {
	ID        int       `json:"id"`
	Firstname string    `form:"firstname" json:"firstname" binding:"required"`
	Lastname  string    `form:"lastname" json:"lastname" binding:"required"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
