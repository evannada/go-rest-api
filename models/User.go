package models

import "time"

type User struct {
	ID        int       `form:"id" json:"id"`
	Username  string    `form:"username" json:"username" binding:"required"`
	RoleId    string    `form:"role_id" json:"role_id" binding:"required"`
	Role      Role      `form:"role_id" json:"role"`
	Password  string    `form:"-" json:"-"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
