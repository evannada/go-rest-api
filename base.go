package main

import (
	"github.com/evannada/go-rest-api/config"
	"github.com/evannada/go-rest-api/controllers"
	"github.com/evannada/go-rest-api/middlewares"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// gin.SetMode(gin.ReleaseMode)
	db := config.DBInit()
	inDB := controllers.InDB{DB: db}
	router := gin.Default()

	v1 := router.Group("api/v1")
	{
		v1.POST("/register", inDB.Register)
		v1.POST("/login", inDB.Login)
		v1.GET("/me", middlewares.Auth, inDB.Me)

		v1.GET("/peoples", middlewares.Auth, inDB.IndexPeople)
		v1.GET("/peoples/:id", middlewares.Auth, inDB.ShowPeople)
		v1.POST("/peoples", middlewares.Auth, inDB.StorePeople)
		v1.PUT("/peoples/:id", middlewares.Auth, inDB.UpdatePeople)
		v1.DELETE("/peoples/:id", middlewares.Auth, inDB.DeletePeople)

		v1.GET("/users", middlewares.Auth, inDB.IndexUser)

	}

	router.Run()
}
