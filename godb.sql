/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : godb

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 11/09/2019 11:35:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for peoples
-- ----------------------------
DROP TABLE IF EXISTS `peoples`;
CREATE TABLE `peoples` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_people_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of peoples
-- ----------------------------
BEGIN;
INSERT INTO `peoples` VALUES (7, '2019-08-12 01:22:18', '2019-08-12 01:22:18', NULL, 'evansd', 'nadsda');
INSERT INTO `peoples` VALUES (8, '2019-08-12 01:22:19', '2019-08-12 01:22:19', NULL, 'evansd', 'nadsda');
INSERT INTO `peoples` VALUES (9, '2019-08-12 01:22:20', '2019-08-12 01:22:20', NULL, 'evansd', 'nadsda');
INSERT INTO `peoples` VALUES (10, '2019-08-12 01:22:20', '2019-08-12 01:22:20', NULL, 'evansd', 'nadsda');
COMMIT;

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `ktp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_profiles_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, 'admin');
INSERT INTO `roles` VALUES (2, 'driver');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_for` (`username`),
  KEY `user_role_id_for` (`role_id`),
  CONSTRAINT `user_role_id_for` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'evannada', 1, '$2a$10$2E6u/XHfqmQutpA5G1DP6uZ/eSxY37vntnX.7qC1X.FxCV6dieXPK', '2019-08-11 23:18:58', '2019-08-11 23:18:58');
INSERT INTO `users` VALUES (5, 'evan2', 1, '$2a$10$Yiv8KrjDcq0ZdPmc8XpHm.jl96hWVVm5NE9TjN6Dozv7/Ug9ikwem', '2019-09-05 18:22:26', '2019-09-05 18:22:26');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
