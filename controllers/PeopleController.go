package controllers

import (
	"net/http"

	"github.com/evannada/go-rest-api/models"
	"github.com/gin-gonic/gin"
)

func (idb *InDB) IndexPeople(c *gin.Context) {
	var (
		peoples []models.People
	)

	idb.DB.Find(&peoples)

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"data":   peoples,
	})
}

func (idb *InDB) ShowPeople(c *gin.Context) {
	var (
		people models.People
	)

	id := c.Param("id")
	err := idb.DB.Where("id = ?", id).First(&people).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"data":   people,
	})
}

func (idb *InDB) StorePeople(c *gin.Context) {
	var people models.People

	if err := c.ShouldBindJSON(&people); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	idb.DB.Create(&people)

	c.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "record updated succesfully.",
		"data":    people,
	})

}

func (idb *InDB) UpdatePeople(c *gin.Context) {
	var (
		people    models.People
		newPeople models.People
	)

	id := c.Param("id")
	err := idb.DB.First(&people, id).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": "record not found.",
		})
		return
	}

	if err := c.ShouldBindJSON(&newPeople); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	idb.DB.Model(&people).Update(newPeople)
	c.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "record updated succesfully.",
		"data":    people,
	})

}

func (idb *InDB) DeletePeople(c *gin.Context) {
	var (
		people models.People
	)

	id := c.Param("id")
	err := idb.DB.First(&people, id).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": "record not found.",
		})
		return
	}

	idb.DB.Delete(&people)
	c.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "record deleted succesfully.",
	})

}
