package controllers

import (
	"net/http"

	"github.com/evannada/go-rest-api/models"
	"github.com/gin-gonic/gin"
)

func (idb *InDB) IndexUser(c *gin.Context) {
	var (
		users []models.User
	)

	idb.DB.Preload("Role").Find(&users)

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"data":   users,
	})
}
