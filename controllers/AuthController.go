package controllers

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/evannada/go-rest-api/models"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type Credential struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type MyClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

func (idb *InDB) Register(c *gin.Context) {
	var (
		credential Credential
		user       models.User
	)

	if err := c.ShouldBindJSON(&credential); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	// Hashing the password with the default cost of 10
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(credential.Password), bcrypt.DefaultCost)

	user.Username = credential.Username
	user.Password = string(hashedPassword)
	user.RoleId = "1"

	if err := idb.DB.Save(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "Succes registered.",
	})

}

func (idb *InDB) Login(c *gin.Context) {
	var (
		credential Credential
		user       models.User
	)

	if err := c.ShouldBindJSON(&credential); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	err := idb.DB.Where("username = ?", credential.Username).First(&user).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(credential.Password))

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"status":  "error",
			"message": "These credentials do not match our records.",
		})
		return
	}

	claims := MyClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "application",
			ExpiresAt: time.Now().Add(time.Hour).Unix(),
		},
		Username: credential.Username,
	}

	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := sign.SignedString([]byte("secret"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		c.Abort()
	}

	idb.DB.Preload("Role").Find(&user)

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"token":  token,
		"data":   user,
	})
}

func (idb *InDB) Me(c *gin.Context) {
	authorizationHeader := c.Request.Header.Get("Authorization")
	bearerToken := strings.Split(authorizationHeader, " ")
	claims, _ := extractClaims(bearerToken[1])

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"data":   claims,
	})

}

func extractClaims(tokenStr string) (jwt.MapClaims, bool) {
	hmacSecret := []byte("secret")
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return hmacSecret, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	}

	log.Printf("Invalid JWT Token")
	return nil, false

}
